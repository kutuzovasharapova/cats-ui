
import { test as base, expect } from '@playwright/test';
import { RatingPage, ratingPageFixture} from '../__page-object__'

const test = base.extend<{ ratingPage: RatingPage }>({
  ratingPage: ratingPageFixture,
});

test('При ошибке сервера в методе raiting - отображается попап ошибки', async ({ page, ratingPage   }) => {
  // Замокать ответ метода получения рейтинга ошибкой на стороне сервера
  await ratingPage.mockErrorRating()

  // Перейти на страницу рейтинга
  await ratingPage.openRatingPage()

  // Проверить, что отображается текст ошибка загрузки рейтинга
  await expect(page.getByText('Ошибка загрузки рейтинга')).toBeVisible();
});

test('Рейтинг котиков отображается', async ({ page, ratingPage  }) => {
  // Перейти на страницу рейтинга
  await ratingPage.openRatingPage()

  // Проверить, что рейтинг количества лайков отображается по убыванию
  const arrayLikes = await ratingPage.getArrayLikes(); 
  const sortedArrayLikes = [...arrayLikes].sort((a, b) => b - a);
            expect(arrayLikes).toEqual(sortedArrayLikes);

});