import type { Page, TestFixture } from '@playwright/test';
import { test } from '@playwright/test';

export class RatingPage {
    private page: Page;
    public likesSelector: string
  
    constructor({ page}: { page: Page;}) {
      this.page = page;
      this.likesSelector = 'td.rating-names_item-count__1LGDH.has-text-success'
    }
  
    async mockErrorRating() {
        return await test.step('Мокирую страницу Рейтинг имен котиков', async () => {
            await this.page.route(
                request => request.href.includes('/cats/rating'),
                async route => {
                await route.fulfill({
                status: 500,
                contentType: 'application/json',
                body: JSON.stringify ({error: 'Internal Server Error', message: 'Ошибка загрузки рейтинга'}),
                });
                }
            );

         }) 
    }

    async openRatingPage() {
      return await test.step('Открываю страницу Рейтинг имен котиков', async () => {
        await this.page.goto('/rating')
      })
    }

    async getArrayLikes() {
        return await test.step('Получаю массив  значений лайков', async () => {
            const elements = await this.page.locator(this.likesSelector).allTextContents();
            
            const arrayLikes = Array.from(elements).map(element => parseInt(element));
            return arrayLikes;
        })
      }
    
}

export type RatingPageFixture = TestFixture<  RatingPage,
  {
    page: Page;
  }
  >;

export const ratingPageFixture: RatingPageFixture = async (
  { page },
  use
) => {
  const ratingPage = new RatingPage({ page });

  await use(ratingPage);
};


    
  